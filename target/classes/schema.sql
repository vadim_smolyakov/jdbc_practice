CREATE TABLE IF NOT EXISTS COMPANIES
(
    id         SERIAL,
    name varchar(100) NOT NULL,
    PRIMARY KEY (id)
);
