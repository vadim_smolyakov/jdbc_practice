package com.company.repositories;

import com.company.database.DataSource;
import com.company.models.Company;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static com.company.database.SqlRequests.CREATE_COMPANIES_TABLE;
import static org.junit.Assert.*;

public class CompanyRepositoryImplTest {
    private Connection connection;
    private CompanyRepositoryImpl companyRepository;
    private MockedStatic<DataSource> dbConnectorMockedStatic;

    @Before
    public void setUp() throws Exception {
//        companyRepository = new CompanyRepositoryImpl();
//        Mockito.when(DBConnector.getConnection()).thenReturn(connection);
        connection = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
        Statement statement = connection.createStatement();
        statement.executeUpdate(CREATE_COMPANIES_TABLE);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void shouldAddWhenCompanyIsNotNull() throws SQLException {
        Company expected = new Company(3,"test");
        companyRepository.add(expected);

//        Mockito.when(DBConnector.getConnection()).thenReturn(connection);
        Company actual = companyRepository.getCompany(3);

        assertEquals(expected,actual);
    }

}