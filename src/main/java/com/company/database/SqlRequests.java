package com.company.database;

public class SqlRequests {
    public final static String CREATE_TRIPS_TABLE =
            "CREATE TABLE IF NOT EXISTS TRIPS (" +
            "ID INT PRIMARY KEY," +
            "COMPANY_ID INT REFERENCES COMPANIES (ID) ON DELETE CASCADE ON UPDATE CASCADE," +
            "TOWN_FROM VARCHAR(255) NOT NULL," +
            "TOWN_TO VARCHAR(255) NOT NULL," +
            "TIME_DEPARTURE TIMESTAMP," +
            "TIME_ARRIVING TIMESTAMP);";

    public final static String CREATE_COMPANIES_TABLE =
            "CREATE TABLE IF NOT EXISTS COMPANIES (" +
                    "ID SERIAL PRIMARY KEY," +
                    "NAME VARCHAR(255) NOT NULL);";

    public final static String DROP_TRIPS_TABLE =
            "DROP TABLE IF EXISTS TRIPS";

    public final static String DROP_COMPANIES_TABLE =
            "DROP TABLE IF EXISTS COMPANIES CASCADE";

    public final static String ADD_TRIP =
            "INSERT INTO " +
                    "TRIPS(ID, COMPANY_ID, TOWN_FROM, TOWN_TO, TIME_DEPARTURE, TIME_ARRIVING)\n" +
                    "VALUES (?, ?, ?, ?, ?, ?)";

    public final static String ADD_COMPANY =
            "INSERT INTO " +
                    "COMPANIES(name) " +
                    "VALUES(?)";
}
