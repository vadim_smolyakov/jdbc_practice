package com.company.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import static com.company.database.SqlRequests.CREATE_COMPANIES_TABLE;
import static com.company.database.SqlRequests.CREATE_TRIPS_TABLE;
import static com.company.database.SqlRequests.DROP_COMPANIES_TABLE;
import static com.company.database.SqlRequests.DROP_TRIPS_TABLE;

public class DataSource {
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(DataSource.class));
    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "root";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public static void createTripsTable() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(CREATE_TRIPS_TABLE);
        } catch (SQLException e) {
            LOGGER.info("Table TRIPS was not created");
        }
    }

    public static void createCompaniesTable() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(CREATE_COMPANIES_TABLE);
        } catch (SQLException e) {
            LOGGER.info("Table COMPANIES was not created");
        }
    }

    public static void dropTripsTable() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(DROP_TRIPS_TABLE);
        } catch (SQLException e) {
            LOGGER.info("Table TRIPS was not deleted");
        }
    }

    public static void dropCompaniesTable() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(DROP_COMPANIES_TABLE);
        } catch (SQLException e) {
            LOGGER.info("Table COMPANIES was not deleted");
        }
    }
}
