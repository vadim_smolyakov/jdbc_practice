package com.company.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Trip {
    private final int id;
    private final Company company;
    private final String townFrom;
    private final String townTo;
    private LocalDateTime dateTimeDeparture;
    private LocalDateTime dateTimeArriving;

    public Trip(int id, Company company, String townFrom, String townTo, LocalDateTime dateTimeDeparture, LocalDateTime dateTimeArriving) {
        this.id = id;
        this.company = company;
        this.townFrom = townFrom;
        this.townTo = townTo;
        this.dateTimeDeparture = dateTimeDeparture;
        this.dateTimeArriving = dateTimeArriving;
    }

    public int getId() {
        return id;
    }

    public Company getCompany() {
        return company;
    }

    public int getCompanyId() {
        return company.getId();
    }

    public String getTownFrom() {
        return townFrom;
    }

    public String getTownTo() {
        return townTo;
    }

    public LocalDateTime getDateTimeDeparture() {
        return dateTimeDeparture;
    }

    public LocalDateTime getDateTimeArriving() {
        return dateTimeArriving;
    }

    public void changeDateTimeDeparture(LocalDateTime dateTimeDeparture) {
        this.dateTimeDeparture = dateTimeDeparture;
    }

    public void changeDateTimeArriving(LocalDateTime dateTimeArriving) {
        this.dateTimeArriving = dateTimeArriving;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return id == trip.id && Objects.equals(company, trip.company) && Objects.equals(townFrom, trip.townFrom) && Objects.equals(townTo, trip.townTo) && Objects.equals(dateTimeDeparture, trip.dateTimeDeparture) && Objects.equals(dateTimeArriving, trip.dateTimeArriving);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, company, townFrom, townTo, dateTimeDeparture, dateTimeArriving);
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", company=" + company +
                ", townFrom='" + townFrom + '\'' +
                ", townTo='" + townTo + '\'' +
                ", dateTimeDeparture=" + dateTimeDeparture +
                ", dateTimeArriving=" + dateTimeArriving +
                '}';
    }
}
