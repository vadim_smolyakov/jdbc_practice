package com.company.repositories;

import com.company.database.DataSource;
import com.company.models.Company;
import com.company.utils.PropertiesUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.company.database.SqlRequests.ADD_COMPANY;

public class CompanyRepositoryImpl {
//    private final PropertiesUtil propertiesUtil;

    public CompanyRepositoryImpl(PropertiesUtil propertiesUtil) {
//        this.propertiesUtil = propertiesUtil;
    }

    public void add(Company company) {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(ADD_COMPANY)) {
            statement.setString(1, company.getName());

            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Logger add(Company company)");
        }
    }

    public Company getCompany(int id) {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("select * from companies where id = ?")) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new Company(resultSet.getInt("id"),resultSet.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println("Logger add(Company company)");
        }
        return null;
    }
}
