package com.company.repositories;

import com.company.models.Trip;

public interface TripRepository {
    boolean add(Trip trip);

    Trip getTrip(int id);

    boolean update(Trip trip);

    boolean contains(Trip trip);

    boolean delete(Trip trip);
}
