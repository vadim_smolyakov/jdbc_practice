package com.company.repositories;

import com.company.database.DataSource;
import com.company.models.Trip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import static com.company.database.SqlRequests.ADD_TRIP;

public class TripRepositoryImpl implements TripRepository {
    @Override
    public boolean add(Trip trip) {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(ADD_TRIP)) {
            statement.setInt(1, trip.getId());
            statement.setInt(2, trip.getCompanyId());
            statement.setString(3, trip.getTownFrom());
            statement.setString(4, trip.getTownTo());
            statement.setTimestamp(5, Timestamp.valueOf(trip.getDateTimeDeparture()));
            statement.setTimestamp(6, Timestamp.valueOf(trip.getDateTimeArriving()));

            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("Logger add(Trip trip)" + e.getMessage());
        }
        return false;
    }

    @Override
    public Trip getTrip(int id) {
        return null;
    }

    @Override
    public boolean update(Trip trip) {
        return false;
    }

    @Override
    public boolean contains(Trip trip) {
        return false;
    }

    @Override
    public boolean delete(Trip trip) {
        return false;
    }
}
