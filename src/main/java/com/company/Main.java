package com.company;

import com.company.database.DataSource;

public class Main {
    public static void main(String[] args) {
//        DBConnector.dropCompaniesTable();
//        DBConnector.dropTripsTable();
        DataSource.createCompaniesTable();
        DataSource.createTripsTable();
//        CompanyRepositoryImpl companyRepository = new CompanyRepositoryImpl();
//        System.out.println(companyRepository.getCompany(1));
    }
}
